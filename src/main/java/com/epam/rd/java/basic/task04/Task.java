package com.epam.rd.java.basic.task04;

public class Task {
	
	public static void main(String[] args) {
		Range range = new Range(3, 10);
		for (Integer el : range) {
			System.out.printf("%d ", el);
		}
		System.out.println();

		System.out.println(range);
		 
		range = new Range(3, 7, true);
		for (Integer el : range) {
			System.out.printf("%d ", el);
		}
		System.out.println();

		System.out.println(range);
	}

}