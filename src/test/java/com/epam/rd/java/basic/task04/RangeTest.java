package com.epam.rd.java.basic.task04;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.*;
import java.util.Arrays;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import com.epam.rd.java.basic.TestHelper;

public class RangeTest {

	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", delimiter = ';')
	void test(int n, int m, Boolean reverse, String expected) throws Exception {
		Constructor<Range> c;
		Range range;
		
		if (reverse != null) {
			c = Range.class.getConstructor(int.class, int.class, boolean.class);
			range = c.newInstance(n, m, reverse);
		} else {
			c = Range.class.getConstructor(int.class, int.class);
			range = c.newInstance(n, m);
		}
		
		StringBuilder sb = new StringBuilder();
		for (Integer el : range) {
			sb.append(el).append(' ');
		}
		String actual = sb.toString().trim();

		TestHelper.logToStdErr(expected, actual);
		assertEquals(expected, actual);
	}

}

